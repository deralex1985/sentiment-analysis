import pandas as pd

def shortify():
    DATASET_COLUMNS = ["sentiment", "ids", "date", "flag", "user", "text"]
    DATASET_ENCODING = "ISO-8859-1"
    dataset = pd.read_csv('data/Sentiment140_1.6mnTweets.csv', encoding=DATASET_ENCODING, names=DATASET_COLUMNS)
    negTweets = dataset[1:1000]
    posTweets = dataset[800001:801000]
    shortList = negTweets.append(posTweets)
    shortList = shortList[['sentiment','text']]
    shortList['sentiment'] = shortList['sentiment'].replace(4, 1)
    shortList['sentiment'] = shortList['sentiment'].replace(0, -1)
    shortList.to_csv('data/Sentiment2000Tweets.csv', index=False)

shortify()