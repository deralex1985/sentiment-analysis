# SOURCE: https://www.kaggle.com/stoicstatic/twitter-sentiment-analysis-for-beginners
import pickle
import time
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import LinearSVC
from wordcloud import WordCloud
from evaluation import model_Evaluate
from preprocessing import preprocess


# Importing the dataset
DATASET_COLUMNS  = ["sentiment", "text"]
DATASET_ENCODING = "ISO-8859-1"
dataset = pd.read_csv('data/Sentiment2000Tweets.csv', encoding=DATASET_ENCODING, names=DATASET_COLUMNS)

print("Sample Tweets")
sample=dataset.sample(n=7, axis=None)
print(sample)

# Plotting the distribution for dataset.
ax = dataset.groupby('sentiment').count().plot(kind='bar', title='Distribution of data',legend=False)
ax.set_xticklabels(['Negative','Positive', ""], rotation=0)
plt.show(block=True)  # Keeps Plot open after code has run, needs to be closed manually to proceed code


# Storing data in lists.
text, sentiment = list(dataset['text']), list(dataset['sentiment'])

#Preprocessing
t = time.time()
processedtext = preprocess(text)
print(f'Text Preprocessing complete.')
print(f'Time Taken: {round(time.time()-t)} seconds')

#SHOW WORD CLOUD FOR NEGATIVE WORDS
data_neg = processedtext[:1000]
plt.figure(figsize = (10,5))
wc = WordCloud(max_words = 1000 , width = 1600 , height = 800,
               collocations=False).generate(" ".join(data_neg))
plt.imshow(wc)
plt.show(block=True)

#SHOW WORD CLOUD FOR POSITIVE WORDS
data_pos = processedtext[1000:2000]
wc = WordCloud(max_words = 1000 , width = 1600 , height = 800,
              collocations=False).generate(" ".join(data_pos))
plt.figure(figsize = (10,5))
plt.imshow(wc)
plt.show(block=True)


#Split Data
X_train, X_test, Y_train, Y_test = train_test_split(processedtext, sentiment, test_size = 0.05, random_state = 0)
print(f'Data Split done.')

#Vectorize
vectoriser = TfidfVectorizer(ngram_range=(1,2), max_features=500000)
vectoriser.fit(X_train)
print('No. of feature_words: ', len(vectoriser.get_feature_names()))
X_train = vectoriser.transform(X_train)
X_test  = vectoriser.transform(X_test)
print(f'Data Transformed.')
print(f'Vectoriser fitted.')

#CREATE MODELS
BNBmodel = BernoulliNB(alpha = 2)
BNBmodel.fit(X_train, Y_train)
model_Evaluate(BNBmodel, X_test, Y_test)

#LinearSVC Model
SVCmodel = LinearSVC()
SVCmodel.fit(X_train, Y_train)
model_Evaluate(SVCmodel, X_test, Y_test)

#Logistic Regression Model
LRmodel = LogisticRegression(C = 2, max_iter = 1000, n_jobs=-1)
LRmodel.fit(X_train, Y_train)
model_Evaluate(LRmodel,X_test, Y_test)

file = open('models/vectoriser-ngram-(1,2).pickle','wb')
pickle.dump(vectoriser, file)
file.close()

file = open('models/Sentiment-LR.pickle','wb')
pickle.dump(LRmodel, file)
file.close()

file = open('models/Sentiment-BNB.pickle','wb')
pickle.dump(BNBmodel, file)
file.close()

