#### This repository is mainly based on a great Kaggle Notebook from Nikit Periwal which can be found here: https://www.kaggle.com/stoicstatic/twitter-sentiment-analysis-for-beginners

#### Get Started:
1) Clone this repository
2) Get the original data source and reduce it to a resonable size. The original resouce - the giantic sentiment140 dataset - is way too large for basic testing and quick changes.
I therefore recommend to reduced the file from 1.6million tweets to 2000 tweets as follows: 

    2.1) Get the original file from one of these sources:
    - [Kaggle](https://www.kaggle.com/kazanova/sentiment140) 
    - [sentiment140.com](http://help.sentiment140.com/for-students)
    
    2.2) Place the file into the project folder "data" and rename it to "Sentiment140_1.6mnTweets.csv"
    
    2.3) Run the file __shortifyFile.py__. You should see a new file in your data folder.
    
3) Run __main.py__ to create and save models. The code stops on plots, so you can inspect the plots one at a time. Close a plot to proceed.
4) Run __predicting.py__ to run sample  prediction
5) Alter the text or the model at the bottom of the __predicting.py__ file to see what happens





